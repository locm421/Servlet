package gameDoanSo;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Guess")
public class Guess extends HttpServlet {
	
@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	String Flag = req.getParameter("Flag");
	int result = Integer.parseInt(req.getParameter("number"));
	int randomNumber = Integer.parseInt(req.getParameter("randomNumber"));
	int count= Integer.parseInt(req.getParameter("count"));
	String namePlayer = req.getParameter("nameplayer");
	String strResult = "2";
	System.out.println(randomNumber);
	if(result > randomNumber)
	{
		strResult = "Số bí ẩn bé hơn số vừa nhập";
		count++;
	}
	else if(result < randomNumber)
	{
		strResult = "Số bí ẩn lớn hơn số vừa nhập";
		count++;
	}
	else
	{
		strResult = "Chúc mừng bạn đã đoán chính xác";
		count++;
		Flag = "2";
	}
	req.setAttribute("count", count);
	req.setAttribute("secretNb", randomNumber);
	req.setAttribute("strResult",strResult);
	req.setAttribute("Flagreq", Flag);
	req.setAttribute("nameplayer", namePlayer);
	req.getRequestDispatcher("Handle.jsp").forward(req, resp);
	System.out.println(count);
}
}
