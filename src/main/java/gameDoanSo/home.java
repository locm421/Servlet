package gameDoanSo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/home")
public class home extends HttpServlet {

	int randomNumber2 = (int) (Math.random() * 1000) + 1;

	private List<Player> list = new ArrayList<Player>();
	String Flag = "0";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("home.html").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String namePlayer = req.getParameter("nameplayer");
		Flag = req.getParameter("Flag");
		int count = Integer.parseInt(req.getParameter("count"));
		Player player = new Player();
		
		if (Flag.equals("0")) {
			req.setAttribute("nameplayer", namePlayer);
			req.setAttribute("player", player);
			req.setAttribute("Flagreq", Flag);
			req.setAttribute("secretNb", player.getValue());
			req.getRequestDispatcher("Handle.jsp").forward(req, resp);

		}
		if (Flag.equals("3")) {
			player.setName(namePlayer);
			player.setMaNguoiChoi("PL" + list.size());
			player.setSoLanDoan(count);
			list.add(player);
			Collections.sort(list, (p1, p2) -> Integer.compare(p1.getSoLanDoan(), p2.getSoLanDoan()));
			req.setAttribute("listPlayer", list);
			req.setAttribute("Flagreq", Flag);
			req.getRequestDispatcher("Handle.jsp").forward(req, resp);
		}
	}

}
