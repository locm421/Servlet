<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="flag" value="${Flagreq}" />
<%--Nhận sự kiện của doPost từ trang home cho lần khởi tạo đầu tiên và chuyển sang sang Guess để
tính toán --%>
	<c:if test="${flag eq '0'}">

		<form action="http://localhost:8080/BaiTapServlet/Guess" method="post">
			<h1 class="context">Mã người chơi ${player.getMaNguoiChoi()}</h1>
			<p1>Số bí ẩn đã được tạo vui lòng nhập sô dự đoán</p1>
			<br> <input type="number" name="number"> <input
				type="hidden" name="randomNumber" value="${player.getValue()}" /> <input
				type="hidden" name="Flag" value="1"> <input type="hidden"
				name="count" value="0"> <input type="hidden"
				name="nameplayer" value="${nameplayer}">
			<button>Nhập</button>
		</form>
	</c:if>
	<%--Gửi nhận dữ liệu cho các lần đoán tiếp theo  --%>
	<c:if test="${flag eq '1'}">
		<form action="http://localhost:8080/BaiTapServlet/Guess" method="post">
			<h1 style="color: red">${strResult}</h>
				<br /> <input type="number" name="number"> <input
					type="hidden" name="randomNumber" value="${secretNb}" /> <input
					type="hidden" name="Flag" value="1"> <input type="hidden"
					name="count" value="${count}"> <input type="hidden"
					name="nameplayer" value="${nameplayer}">
				<button>Nhập</button>
		</form>
	</c:if>
	<%--CHO PHÉP NGƯỜI CHƠI LỰA CHỌN CHƠI TIẾP HOẶC XEM BẢNG XẾP HẠNG SAU KHI ĐOÁN ĐÚNG --%>
	<c:if test="${flag eq '2'}">
		<form action="http://localhost:8080/BaiTapServlet/home" method="get">
			<h1 style="color: green">${strResult}</h1>
			<br /> <input type="hidden" name="action" vlaue="PlayAgain">
			<input type="hidden" name="count" value="0">
			<button>Chơi tiếp</button>
		</form>
		<form action="http://localhost:8080/BaiTapServlet/home" method="post">
			<input type="hidden" name="action" vlaue="viewLeaderboard"> <input
				type="hidden" name="Flag" value="3"> <input type="hidden"
				name="count" value="${count}"> <input type="hidden"
				name="nameplayer" value="${nameplayer}">
			<button>Xem bảng xếp hạng</button>
		</form>
	</c:if>
	
	
	<%--TẠO BẢNG XẾP HẠNG --%>
	<c:if test="${flag eq '3'}">
		<form action="http://localhost:8080/BaiTapServlet/home" method="get">
			<h1>Leaderboard</h1>
			<table border="1">
				<tr>
					<th>Rank</th>
					<th>Code PLayer</th>
					<th>Name</th>
					<th>Score</th>
				</tr>
				<c:forEach items="${listPlayer}" var="player" varStatus="status">
					<tr>
						<td>${status.index + 1}</td>
						<td>${player.getMaNguoiChoi()}</td>
						<td>${player.getName()}</td>
						<td>${player.getSoLanDoan()}</td>
					</tr>
				</c:forEach>
			</table>
			<input type="hidden" name="action" vlaue="PlayAgain"> <input
				type="hidden" name="count" value="0"><br />
			<button>Home</button>
		</form>
	</c:if>

</body>
</html>